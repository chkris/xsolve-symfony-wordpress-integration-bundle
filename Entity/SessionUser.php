<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Holds reference to Session and User ids
 *
 * @ORM\Table(name="session_user")
 * @ORM\Entity()
 */
class SessionUser
{
   /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
     * @ORM\Column(name="user_id", type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(name="session_id", type="string")
     */
    protected $sessionId;

    /**
     * @param integer $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }
}
