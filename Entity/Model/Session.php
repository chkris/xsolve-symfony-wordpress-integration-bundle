<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Session
{
    /**  @ORM\Id @ORM\Column(type="integer") GeneratedValue */
    protected $id;

    /** @ORM\Column() */
    protected $value;

    /** @ORM\Column() */
    protected $time;

    /**
     * @param string $id
     * @param string $value
     * @param string $time
     */
    public function __construct($id, $value, $time)
    {
        $this->id = $id;
        $this->value = $value;
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param string $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }
}
