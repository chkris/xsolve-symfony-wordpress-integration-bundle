<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult;

interface WordpressAuthenticationResponseBuilderInterface
{
    /**
     * @param WordpressAuthenticationResult $wordpressAuthenticationBulider
     * @retrun WordpressLoginResponseObject
     */
    public function buildResponse(WordpressAuthenticationResult $wordpressAuthenticationBulider);
}
