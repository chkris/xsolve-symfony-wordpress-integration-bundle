<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy\WordpressAuthentiationOkResponseBuilder,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy\WordpressAuthentiationFailResponseBuilder;

class WordpressAuthenticationResponseBuilder implements WordpressAuthenticationResponseBuilderInterface
{
    /**
     * @param WordpressAuthenticationResult $wordpressAuthenticationBulider
     * @retrun WordpressLoginResponseObject
     */
    public function buildResponse(WordpressAuthenticationResult $wordpressAuthenticationResult)
    {
        $wordpressAuthentiationResponseBuilder = null;

        if ($wordpressAuthenticationResult->getIsValid()) {
            $wordpressAuthentiationResponseBuilder = new WordpressAuthentiationOkResponseBuilder;
        } else {
            $wordpressAuthentiationResponseBuilder = new WordpressAuthentiationFailResponseBuilder;
        }

        return $wordpressAuthentiationResponseBuilder->buildResponse($wordpressAuthenticationResult);
    }
}
