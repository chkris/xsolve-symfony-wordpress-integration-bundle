<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult;

interface WordpressAuthenticationResponseBuilderInterface
{
    public function buildResponse(WordpressAuthenticationResult $wordpressAuthenticationBulider);
}
