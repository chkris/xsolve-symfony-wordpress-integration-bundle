<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy\WordpressAuthenticationResponseBuilderInterface,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Model\WordpressLoginResponseObject;

class WordpressAuthentiationFailResponseBuilder implements WordpressAuthenticationResponseBuilderInterface
{
    public function buildResponse(WordpressAuthenticationResult $wordpressAuthenticationResult)
    {
        $wordpressLoginResponseObject = new WordpressLoginResponseObject();

        $wordpressLoginResponseObject->setResult(WordpressLoginResponseObject::STATUS_FAILURE);

        return $wordpressLoginResponseObject;
    }
}
