<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Builder\Strategy\WordpressAuthenticationResponseBuilderInterface,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Model\WordpressLoginResponseObject;

class WordpressAuthentiationOkResponseBuilder implements WordpressAuthenticationResponseBuilderInterface
{
    public function buildResponse(WordpressAuthenticationResult $wordpressAuthenticationResult)
    {
        $wordpressLoginResponseObject = new WordpressLoginResponseObject();

        $wordpressLoginResponseObject->setResult(WordpressLoginResponseObject::STATUS_CORRECT)
            ->setUsername($wordpressAuthenticationResult->getUser()->getUsername())
            ->setFirstName($wordpressAuthenticationResult->getUser()->getFirstname())
            ->setLastName($wordpressAuthenticationResult->getUser()->getLastname())
            ->setEmail($wordpressAuthenticationResult->getUser()->getEmail());

        return $wordpressLoginResponseObject;
    }
}
