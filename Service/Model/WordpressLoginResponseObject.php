<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Model;

use JsonSerializable;

class WordpressLoginResponseObject implements JsonSerializable
{
    const STATUS_CORRECT = 1;
    const STATUS_FAILURE = 0;

   /**
    * @var string
    */
    protected $result;

    /**
    * @var string
    */
    protected $username;

    /**
    * @var string
    */
    protected $firstName;

   /**
    * @var string
    */
    protected $lastName;

   /**
    * @var string
    */
    protected $email;

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param  string                       $result
     * @return WordpressLoginResponseObject
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @param  string                       $username
     * @return WordpressLoginResponseObject
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param  string                       $firstName
     * @return WordpressLoginResponseObject
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param  string                       $lastName
     * @return WordpressLoginResponseObject
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param  string                       $email
     * @return WordpressLoginResponseObject
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'result'     => $this->result,
            'username'   => $this->username,
            'first_name' => $this->firstName,
            'last_name'  => $this->lastName,
            'email'      => $this->email
        );
    }
}
