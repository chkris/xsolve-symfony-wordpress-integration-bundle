<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler;

use Symfony\Component\HttpFoundation\Request;

interface WordpressAuthenticationHandlerInterface
{
    /**
     * @param Symfony\Component\HttpFoundation\Request $request
     */
    public function handleRequest(Request $request);

    /**
     * @return Object
     */
    public function getResult();
}
