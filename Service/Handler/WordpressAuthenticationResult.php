<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler;

class WordpressAuthenticationResult
{
    protected $isValid;
    protected $user;

    public function __construct($isValid, $user)
    {
        $this->isValid = $isValid;
        $this->user= $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getIsValid()
    {
        return $this->isValid;
    }

    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    }

}
