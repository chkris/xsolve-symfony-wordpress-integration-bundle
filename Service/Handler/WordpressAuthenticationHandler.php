<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler;

use Symfony\Component\HttpFoundation\Request;

use Doctrine\ORM\EntityManager;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Validator\WordpressAuthenticationValidatorInterface,
    Xsolve\SymfonyWordpressIntegrationBundle\Repository\SessionRepository,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\WordpressAuthenticationResult;

class WordpressAuthenticationHandler implements WordpressAuthenticationHandlerInterface
{
    /**
     * @var Object $result
     */
    protected $result;

    /**
     * @var Doctrine\ORM\EntityManager $validator
     */
    protected $validator;

    /**
     * @var Doctrine\ORM\EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @var Doctrine\ORM\EntityManager $sessionRepository
     */
    protected $sessionRepository;

    /**
     * @var WordpressAuthenticationExceptionHandler
     */
    protected $wordpressAuthenticationExceptionHandler;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(
        EntityManager $entityManager,
        WordpressAuthenticationValidatorInterface $validator,
        SessionRepository $sessionRepository,
        WordpressAuthenticationExceptionHandler $wordpressAuthenticationExceptionHandler)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->sessionRepository = $sessionRepository;
        $this->wordpressAuthenticationExceptionHandler = $wordpressAuthenticationExceptionHandler;
    }

    /**
     * @param  Symfony\Component\HttpFoundation\Request $request
     * @return Object
     */
    public function handleRequest(Request $request)
    {

            $sessionId = $request->request->get('sessionId');
            $sessionUser = $this->getSessionUserRepository()->findOneBySessionId($sessionId);

            try {
                $this->validator->validate(
                    $this->createValidatorContext($sessionId, $sessionUser)
                );
            } catch (Exception $exception) {
                $this->wordpressAuthenticationExceptionHandle->handle($exception);

                return new WordpressAuthenticationResult(false, null);
            }

            return new WordpressAuthenticationResult(true, $this->getUserRepository()->find($sessionUser->getUserId()));
    }

    /**
     * @return Object
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository()
    {
        return $this->entityManager->getRepository('KsbBackendBundle:User');
    }

    /**
     * @return UserRepository
     */
    protected function getSessionUserRepository()
    {
        return $this->entityManager->getRepository('XsolveSymfonyWordpressIntegrationBundle:SessionUser');
    }

    /**
     * @param  string      $sessionId
     * @param  SessionUser $sessionId
     * @return array
     */
    protected function createValidatorContext($sessionId, $sessionUser)
    {
        return array(
            'sessionUser' => $sessionUser,
            'session' => $this->sessionRepository->findBySessionId($sessionId),
            'user' => $this->getUserRepository()->find($sessionUser->getUserId())
        );
    }
}
