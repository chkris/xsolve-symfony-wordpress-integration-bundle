<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception;

use Exception;

class NoSessionExistException extends Exception
{
    const MESSAGE = 'No session entry exist exception';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
