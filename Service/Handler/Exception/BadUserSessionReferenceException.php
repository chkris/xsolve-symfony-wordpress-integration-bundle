<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception;

use Exception;

class BadUserSessionReferenceException extends Exception
{
    const MESSAGE = 'No user to session reference exception';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }

}
