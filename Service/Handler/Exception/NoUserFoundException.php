<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception;

use Exception;

class NoUserFoundException extends Exception
{
    const MESSAGE = 'No user exist exception';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
