<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception;

use Exception;

class BadSessionIdException extends Exception
{
    const MESSAGE = 'Bad session id exception';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
