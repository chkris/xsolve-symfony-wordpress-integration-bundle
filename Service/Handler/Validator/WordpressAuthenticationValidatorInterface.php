<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Validator;

interface WordpressAuthenticationValidatorInterface
{
    /**
     * @param array $contextn
     */
    public function validate($context);
}
