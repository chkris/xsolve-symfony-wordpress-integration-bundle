<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Validator;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadUserSessionReferenceException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoSessionExistException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadSessionIdException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoUserFoundException;

class WordpressAuthenticationValidator implements WordpressAuthenticationValidatorInterface
{
    /**
     * @param array $context
     * @throw BadUserSessionReferenceException
     * @throw NoSessionExistException
     * @throw BadSessionIdException
     * @throw NoUserFoundException
     */
    public function validate($context)
    {
        $sessionUser = $context['sessionUser'];
        $session = $context['session'];
        $user = $context['user'];

        if (null == $sessionUser) {
           throw new BadUserSessionReferenceException();
        }

        if ($session instanceof Session) {
            throw new NoSessionExistException();
        }

        if ($session->getId() != $sessionUser->getSessionId()) {
            throw new BadSessionIdException();
        }

        if (null == $user) {
            throw new NoUserFoundException();
        }
    }
}
