<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler;

use Symfony\Bridge\Monolog\Logger;

class WordpressAuthenticationExceptionHandler
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Exception $exception
     */
    public function handle(Exception $exception)
    {
        if ($exception instanceof BadUserSessionReferenceException) {
            $this->logger()->err($exception->getMessage());
        }

        if ($exception instanceof NoSessionExistException) {
            $this->logger->err($exception->getMessage());
        }

        if ($exception instanceof NoUserFoundException) {
            $this->logger->err($exception->getMessage());
        }

        if ($exception instanceof Exception) {
            $this->logger->err($exception->getMessage());
        }
    }
}
