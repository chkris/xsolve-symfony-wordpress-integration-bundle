
XSolve Symfony-Worpress Integration Bundle
===========================

Description

Features include:

- 1
- 2

## Author

[XSolve](mailto:office@xsolve.pl)

# Installation

1) Add to composer.json

    "require": {
        "xsolve-pl/xsolve-symfony-wordpress-integration-bundle" : "dev-master"
    },

2) Install dependencies

    composer install

3) Run the bundle in app/AppKernel.php

    public function registerBundles()
    {
        return array(
            // ...
            new Xsolve\SymfonyWordpressIntegrationBundle\XsolveSymfonyWordpressIntegrationBundle(),
        );
    }

### Update your database schema

Bundle provide set of entities, which should be mapped to database schema. To
update schema run:

    ./app/console doctrine:schema:update --force

### Import routings

To work with delivered controllers you need to import routings in your main
routing.yml. 

# app/config/routing.yml
XsolveSymfonyWordpressIntegrationBundle_wordpress:
    resource: "@XsolveSymfonyWordpressIntegrationBundle/Resources/config/routing/routing.yml"
    prefix:   /  

### Set up the bundle

Setup session to be stored in database and cookie domain as shown in configuration below.
The proper subdomain configuration example is as follow:
blog.symfony_app.xsolve.pl - subdomain address of the wordpress blog
symfony_app.xsolve.pl - subdomain address of the Symfony application

    # app/config/config.yml
    session:
         cookie_domain: .chkris.xsdev.pl # put here your domain, it is required to be session sharable between subdomains 
         handler_id: session.storage.pdo

## Tests

Unit tests can be run with:

    phpunit -c backend vendor/xsolve-pl/xsolve-symfony-wordpress-integration-bundle/Xsolve/SymfonyWordpressIntegrationBundle/

