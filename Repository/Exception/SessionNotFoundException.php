<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Repository\Exception;

use Doctrine\ORM\NoResultException;

class SessionNotFoundException extends NoResultException
{
    const MESSAGE = 'Session entry not found';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
