<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Repository;

use Doctrine\ORM\EntityManager,
    Doctrine\ORM\Query\ResultSetMapping;

use Xsolve\SymfonyWordpressIntegrationBundle\Model\Session,
    Xsolve\SymfonyWordpressIntegrationBundle\Repository\Exception\SessionNotFoundException;

class SessionRepository
{
    /**
     * @var Doctrine\ORM\EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @var array $options
     */
    protected $options;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param array                       $options
     */
    public function __construct(EntityManager $entityManager, array $options)
    {
        $this->entityManager = $entityManager;
        $this->options = $options;
    }

    /**
     * @param string $sessionId
     * @throw SessionNotFoundException
     */
    public function findBySessionId($sessionId)
    {

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Xsolve\SymfonyWordpressIntegrationBundle\Entity\Model\Session', 's');
        $rsm->addFieldResult('s', $this->options['db_id_col'], 'id');
        $rsm->addFieldResult('s', $this->options['db_data_col'], 'value');
        $rsm->addFieldResult('s', $this->options['db_time_col'], 'time');

        $sql = sprintf('SELECT * FROM %s WHERE %s = :sessionId',
                $this->options['db_table'],
                $this->options['db_id_col']
             );

        $query = $this->entityManager->createNativeQuery($sql, $rsm);

        $query->setParameter("sessionId", $sessionId);

        $result = $query->getSingleResult();

        if (false === $result) {
            throw new SessionNotFoundException();
        }

        return $result;
    }
}
