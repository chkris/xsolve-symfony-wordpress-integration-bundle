<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\JsonResponse;

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadUserSessionReferenceException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoSessionExistException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadSessionIdException,
    Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoUserFoundException;

class WordpressController extends Controller
{
    /**
     * @Route("/wordpress-authentication", name="wordpress_authentication")
     */
    public function wordpressAuthenticationAction(Request $request)
    {
        return new JsonResponse($this->getBuilder()->buildResponse(
            $this->getWordpressAuthenticationHandler()->handleRequest($request)
        ));
    }

    /**
     * @return Logger
     */
    protected function getLogger()
    {
        return $this->get('logger');
    }

    /**
     * @return WordpressAuthenticationResponseBuilderInterface
     */
    protected function getBuilder()
    {
        return $this->get('xsolve.symfony_wordpress_integration.wordpress_authentication_response_builder');
    }

    /**
     * @return WordpressAuthenticationHandlerInterface
     */
    protected function getWordpressAuthenticationHandler()
    {
        return $this->get('xsolve.symfony_wordpress_integration.wordpress_authentication_handler');
    }
}
