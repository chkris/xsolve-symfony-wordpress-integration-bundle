<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand,
    Symfony\Component\Console\Input\InputInterface,
    Symfony\Component\Console\Output\OutputInterface;

class CreateSessionTableCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('session:table:create')
            ->setDescription('Create table in database for storing session. Support only mysql.');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stmt = $this->getContainer()->get('doctrine')->getManager()
            ->getConnection()
            ->prepare(
                sprintf(
                    'CREATE TABLE `%s` (
                         `%s` varchar(255) NOT NULL,
                         `%s` text NOT NULL,
                         `%s` int(11) NOT NULL,
                         PRIMARY KEY (`%s`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;',
                    $this->getContainer()->parameters['pdo.session_db_options']['db_table'],
                    $this->getContainer()->parameters['pdo.session_db_options']['db_id_col'],
                    $this->getContainer()->parameters['pdo.session_db_options']['db_data_col'],
                    $this->getContainer()->parameters['pdo.session_db_options']['db_time_col'],
                    $this->getContainer()->parameters['pdo.session_db_options']['db_id_col']
                )
            );

        try {
            $stmt->execute();
        } catch (\Exception $exception) {
            $output->writeln(sprintf('Cannot create table in database: %s', $exception->getMessage()));
        }

        $output->writeln('Table created successfully');
    }
}
