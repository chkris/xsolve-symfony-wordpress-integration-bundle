<?php

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Validator\WordpressAuthenticationValidator;

require_once(__DIR__ . '/FakeUser.php');

class WordpressAuthenticationValidatorTest  extends \PHPUnit_Framework_TestCase
{
    protected $session;
    protected $sessionUser;
    protected $user;
    protected $wordpressAuthenticationValidator;

    public function setUp()
    {
        $this->session = $this->getMockBuilder('Xsolve\SymfonyWordpressIntegrationBundle\Entity\Model\Session')
             ->disableOriginalConstructor()
             ->getMock();
        $this->session->expects($this->any())
             ->method('getId')
             ->will($this->returnValue('uhjdu81b333d5pa6ptv506fuu7'));

        $this->sessionUser = $this->getMock('Xsolve\SymfonyWordpressIntegrationBundle\Entity\SessionUser');
        $this->sessionUser->expects($this->any())
             ->method('getSessionId')
             ->will($this->returnValue('uhjdu81b333d5pa6ptv506fuu7'));

        $this->user = $this->getMock('FakeUser');

        $this->wordpressAuthenticationValidator = new WordpressAuthenticationValidator();
    }

    public function  testValidateCorrect()
    {
        $context = array(
            'sessionUser' => $this->sessionUser,
            'session' => $this->session,
            'user' => $this->user
        );

        $result = true;
        try {
            $this->wordpressAuthenticationValidator->validate($context);
        } catch (Exception $e) {
            $result = false;
        }

        $this->assertEquals(true, $result);
    }

    public function  testValidateFailureWhenUserIsNull()
    {
        $context = array(
            'sessionUser' => $this->sessionUser,
            'session' => $this->session,
            'user' => null
        );

        $result = true;
        try {
            $this->wordpressAuthenticationValidator->validate($context);
        } catch (Exception $e) {
            $result = false;
        }

        $this->assertEquals(false, $result);
    }
}
