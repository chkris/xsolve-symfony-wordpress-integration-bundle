<?php

use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadSessionIdException;
use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadUserSessionReferenceException;
use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoSessionExistException;
use Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoUserFoundException;
use Xsolve\SymfonyWordpressIntegrationBundle\Repository\Exception\SessionNotFoundException;

class ExceptionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException         Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadSessionIdException
     * @expectedExceptionMessage Bad session id exception
     */
    public function testBadSessionIdException()
    {
        throw new BadSessionIdException();
    }

    /**
     * @expectedException         Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\BadUserSessionReferenceException
     * @expectedExceptionMessage No user to session reference exception
     */
    public function testBadUserSessionReferenceException()
    {
        throw new BadUserSessionReferenceException();
    }

    /**
     * @expectedException         Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoSessionExistException
     * @expectedExceptionMessage No session entry exist exception
     */
    public function testNoSessionExistException()
    {
        throw new NoSessionExistException();
    }

    /**
     * @expectedException         Xsolve\SymfonyWordpressIntegrationBundle\Service\Handler\Exception\NoUserFoundException
     * @expectedExceptionMessage No user exist exception
     */
    public function testNoUserFoundException()
    {
        throw new NoUserFoundException();
    }

    /**
     * @expectedException         Xsolve\SymfonyWordpressIntegrationBundle\Repository\Exception\SessionNotFoundException
     * @expectedExceptionMessage No result was found for query although at least one row was expected.
     */
    public function testSessionNotFoundException()
    {
        throw new SessionNotFoundException();
    }
}
