<?php

namespace Xsolve\SymfonyWordpressIntegrationBundle\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;

use Doctrine\ORM\EntityManager;

use Xsolve\SymfonyWordpressIntegrationBundle\Entity\SessionUser;

class LoginListener
{
    /**
     * @var Symfony\Component\Security\Core\SecurityContext $securityContext
     */
    protected $securityContext;

    /**
     * @var Doctrine\ORM\EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @param \Symfony\Component\Security\Core\SecurityContext $securityContext
     * @param \Doctrine\ORM\EntityManager                      $entityManager
     */
    public function __construct(SecurityContext $securityContext, EntityManager $entityManager)
    {
        $this->securityContext = $securityContext;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (null != $user) {
            if ($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ||
                $this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

                $sessionUser = new SessionUser();

                $sessionUser->setUserId($user->getId());
                $sessionUser->setSessionId($event->getRequest()->getSession()->getId());

                $this->entityManager->persist($sessionUser);
            }

            $this->entityManager->flush();
        }
     }
}
